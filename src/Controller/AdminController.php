<?php

namespace App\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Link;
use App\Entity\Path;
use App\Entity\Project;
use App\Form\LinkType;
use App\Form\PathType;
use App\Form\ProjectType;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="admin")
     * @IsGranted("ROLE_USER")
     */
    public function adminAction()
    {
        $em = $this->getDoctrine()->getManager();
        $paths = $em->getRepository('App:Path')->findAll();
        $projects = $em->getRepository('App:Project')->findAll();
        $links = $em->getRepository('App:Link')->findAll();
        return $this->render('admin/admin.html.twig', [
            'paths' => $paths,
            'projects' => $projects,
            'links' => $links
        ]);
    }

    /**
     * @Route("/add/path", name="add_path")
     */
    public function addPathAction(Request $request)
    {
        $path = new Path();
        $form = $this->createForm(PathType::class, $path);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($path);
            $em->flush();
            $this->addFlash('add_path', 'Le parcours a bien été ajouté');
            return $this->redirectToRoute('homepage');
        }
        return $this->render(':form:path.html.twig', [
            'form'=> $form->createView()
        ]);
    }

    /**
     * @Route("/edit/path/{path}", name="edit_path")
     */
    public function editPathAction(Request $request, Path $path)
    {
        $form = $this->createForm(PathType::class, $path);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($form->getData());
            $em->flush();
        }
        return $this->render(':form:path.html.twig', [
           'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/add/project", name="add_project")
     */
    public function addProjectAction(Request $request)
    {
        $project = new Project();
        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();
            $this->addFlash('add_project', 'Le projet a bien été ajouté');
            return $this->redirectToRoute('homepage');
        }
        return $this->render('form/project.html.twig', [
            'form'=> $form->createView()
        ]);
    }

    /**
     * @Route("/edit/project/{project}", name="edit_project")
     */
    public function editProjectAction(Request $request, Project $project)
    {
        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($form->getData());
            $em->flush();
        }
        return $this->render(':form:project.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/add/link", name="add_link")
     */
    public function addLinkAction(Request $request)
    {
        $link = new Link();
        $form = $this->createForm(LinkType::class, $link);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($link);
            $em->flush();
            $this->addFlash('add_link', 'Le lien a bien été ajouté');
            return $this->redirectToRoute('homepage');
        }
        return $this->render('form/link.html.twig', [
            'form'=> $form->createView()
        ]);
    }

    /**
     * @Route("/edit/link/{link}", name="edit_link")
     */
    public function editLinkAction(Request $request, Link $link)
    {
        $form = $this->createForm(LinkType::class, $link);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($form->getData());
            $em->flush();
        }
        return $this->render('form/link.html.twig', [
           'form' => $form->createView()
        ]);
    }
}
